# Game Tools Library

[![Netlify Status](https://api.netlify.com/api/v1/badges/48762823-2dbd-4c5a-b506-0d85de27647d/deploy-status)](https://app.netlify.com/sites/game-tools/deploys)

[Game-Tools](https://game-tools.netlify.app/)

## SVG

### Add new svg

if you'r adding new svg, you need to run this cmd to transform svg into react component (for more info see this [Blog - Svgr](https://gregberge.com/fr/blog/svg-to-react-component-with-svgr) or [Git - gregberge/svgr](https://github.com/gregberge/svgr))

```
npm run build:svg
```

### Create svg from img

[Pixels to Svg](https://codepen.io/shshaw/pen/XbxvNj)
