import React from "react";
import Hearts from "./Hearts";

export default {
  title: "Health bar",
  component: Hearts,
};

export const example = () => <Hearts hearts={2} maxHearts={5} />;
export const fullLife = () => <Hearts hearts={5} maxHearts={5} />;
export const criticalLife = () => <Hearts hearts={0.5} maxHearts={5} />;
