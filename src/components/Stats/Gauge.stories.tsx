import React from "react";
import Gauge from "./Gauge";

export default {
  title: "Custom Gauge",
  component: Gauge,
};

export const example = () => <Gauge actual={85} max={100} />;
export const halfGauge = () => <Gauge actual={50} max={100} />;
export const fullGauge = () => <Gauge actual={100} max={100} />;
export const criticalGauge = () => <Gauge actual={1} max={100} />;
