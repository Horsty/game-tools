import React from "react";
import Inventory from "./Inventory";
import InventorySlot from "../Slot/Slot";
import SvgLaptop from "../Objects/Laptop";
import SvgSword from "../Objects/Sword";
import SvgGameboy from "../Objects/Gameboy";
import SvgShield from "../Objects/Shield";
import SvgBow from "../Objects/Bow";
import SvgControllerNes from "../Objects/ControllerNes";
import SvgMap from "../Objects/Map";
import SvgSwordLarge from "../Objects/SwordLarge";

export default {
  title: "Inventory",
  component: Inventory,
};

const fullItems = {
  helmet: () =>
    InventorySlot({
      item: () => SvgSwordLarge({ width: 320 }),
      name: "Large Sword",
    }),
  legs: () =>
    InventorySlot({ item: () => SvgMap({ width: 320 }), name: "Map" }),
  belt: () =>
    InventorySlot({
      item: () => SvgControllerNes({ width: 320 }),
      name: "Controller Nes",
    }),
  accessory: () =>
    InventorySlot({ item: () => SvgBow({ width: 320 }), name: "Bow" }),
  principalWeapon: () =>
    InventorySlot({ item: () => SvgSword({ width: 320 }), name: "Sword" }),
  tool: () =>
    InventorySlot({ item: () => SvgLaptop({ width: 320 }), name: "Laptop" }),
  goodies: () =>
    InventorySlot({ item: () => SvgGameboy({ width: 320 }), name: "GameBoy" }),
  secondaryWeapon: () =>
    InventorySlot({ item: () => SvgShield({ width: 320 }), name: "Shield" }),
};

export const FullItems = () => <Inventory {...fullItems} />;
export const NoItems = () => <Inventory />;
export const PartielItems = () => (
  <Inventory
    goodies={fullItems.goodies}
    belt={fullItems.belt}
    legs={fullItems.legs}
  />
);
