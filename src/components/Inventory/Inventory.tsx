import React from "react";
import styles from "./Inventory.module.scss";
import PropTypes from "prop-types";

const Inventory = ({
  helmet,
  legs,
  belt,
  accessory,
  principalWeapon,
  tool,
  goodies,
  secondaryWeapon,
}) => {
  return (
    <div className={`${styles.layout} ${styles.wrap}`}>
      <div className={`${styles.inventoryCard} ${styles["card-a"]}`}>
        {helmet ? helmet() : "no items"}
      </div>
      <div
        className={`${styles.inventoryCard} ${styles["card-b"]} ${styles.large}`}
      >
        {principalWeapon ? principalWeapon() : "no items"}
      </div>
      <div className={`${styles.inventoryCard} ${styles["card-c"]}`}>
        {secondaryWeapon ? secondaryWeapon() : "no items"}
      </div>
      <div className={`${styles.inventoryCard} ${styles["card-d"]}`}>
        {belt ? belt() : "no items"}
      </div>
      <div className={`${styles.inventoryCard} ${styles["card-e"]}`}>
        {tool ? tool() : "no items"}
      </div>
      <div className={`${styles.inventoryCard} ${styles["card-f"]}`}>
        {legs ? legs() : "no items"}
      </div>
      <div className={`${styles.inventoryCard} ${styles["card-g"]}`}>
        {goodies ? goodies() : "no items"}
      </div>
      <div className={`${styles.inventoryCard} ${styles["card-h"]}`}>
        {accessory ? accessory() : "no items"}
      </div>
    </div>
  );
};

Inventory.defaultProps = {
  helmet: null,
  legs: null,
  belt: null,
  accessory: null,
  principalWeapon: null,
  tool: null,
  goodies: null,
  secondaryWeapon: null,
};
Inventory.prototype = {
  helmet: PropTypes.func,
  legs: PropTypes.func,
  belt: PropTypes.func,
  accessory: PropTypes.func,
  principalWeapon: PropTypes.func,
  tool: PropTypes.func,
  goodies: PropTypes.func,
  secondaryWeapon: PropTypes.func,
};
export default Inventory;
