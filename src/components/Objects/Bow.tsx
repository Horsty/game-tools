import * as React from "react";

function SvgBow(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 -0.5 32 32"
      shapeRendering="crispEdges"
      {...props}
    >
      <path
        stroke="#4c262b"
        d="M14 0h2m-3 1h1m1 0h1m-3 1h1m1 0h1m-4 1h1m1 0h1m-4 1h1m2 0h1m-4 1h1m1 0h1m-4 1h1m2 0h1M9 7h1m2 0h1M8 8h1m2 0h1m12 0h1M7 9h1m3 0h1m12 0h1M7 10h1m2 0h1m13 0h1M6 11h1m3 0h1m13 0h1M6 12h1m2 0h1m14 0h1M5 13h1m3 0h1m14 0h1M5 14h1m3 0h1m14 0h1M5 15h1m3 0h1m14 0h1M5 16h1m3 0h1m14 0h1M5 17h1m3 0h1m14 0h1M5 18h1m3 0h1m14 0h1M6 19h1m2 0h1m14 0h1M6 20h1m3 0h1m13 0h1M7 21h1m2 0h1m13 0h1M7 22h1m3 0h1m12 0h1M8 23h1m2 0h1m12 0h1M9 24h1m2 0h1m11 0h1m-15 1h1m2 0h1m10 0h1m-14 1h1m1 0h1m10 0h1m-14 1h1m2 0h1m9 0h1m-13 1h1m1 0h1m9 0h1m-12 1h1m1 0h1m-3 1h1m1 0h1m-2 1h2"
      />
      <path
        stroke="#744638"
        d="M14 1h1m-3 3h1m-2 2h1m-2 1h1M9 8h1M8 9h2m-2 1h1m-2 1h2m-2 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h2m-1 1h1m-1 1h2m-1 1h1m0 1h1m0 1h1m0 2h1m1 3h1"
      />
      <path
        stroke="#733e39"
        d="M14 2h1m-2 1h1m-1 1h1m-2 1h1m-1 1h1m-2 1h1m-2 1h1m-1 1h1m-2 1h1m-1 1h1m-2 1h1m-3 1h1m1 0h1m-3 1h1m1 0h1m-3 1h1m1 0h1m-3 1h1m1 0h1m-3 1h1m1 0h1m-3 1h1m1 0h1m-1 1h1m0 1h1m-1 1h1m0 1h1m-1 1h1m0 1h1m0 1h1m-1 1h1m0 1h1m-1 1h1m0 1h1"
      />
      <path
        stroke="#181425"
        d="M15 3h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1"
      />
      <path
        stroke="#686c6e"
        d="M14 5h1m9 0h1M14 6h1m8 0h3M14 7h1m8 0h1m1 0h1M14 8h1m7 0h2m1 0h2M14 9h1m7 0h1m3 0h1m-13 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m8 0h1m1 0h1m-12 1h1m8 0h1m1 0h1m-12 1h1m7 0h2m1 0h2"
      />
      <path stroke="#193c3e" d="M24 7h1m-2 20h1m1 0h1" />
    </svg>
  );
}

export default SvgBow;
