import * as React from "react";

function SvgShield(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 -0.5 32 32"
      shapeRendering="crispEdges"
      {...props}
    >
      <path
        stroke="#000"
        d="M5 5h23M5 6h1m21 0h1M5 7h1m21 0h1M5 8h1m21 0h1M5 9h1m21 0h1M5 10h1m21 0h1M5 11h1m21 0h1M6 12h1m19 0h1M6 13h1m19 0h1M6 14h1m19 0h1M6 15h1m19 0h1M7 16h1m17 0h1M7 17h1m17 0h1M7 18h1m17 0h1M8 19h1m15 0h1M8 20h1m15 0h1M8 21h1m15 0h1M9 22h1m13 0h1m-14 1h1m11 0h1m-12 1h1m9 0h1m-10 1h1m7 0h1m-8 1h1m5 0h1m-6 1h5"
      />
      <path
        stroke="#a22633"
        d="M6 6h1m2 0h1m2 0h1m2 0h1m2 0h1m2 0h1m2 0h1M8 7h1m2 0h1m2 0h1m2 0h1m2 0h1m2 0h1M7 8h1m2 0h1m2 0h1m2 0h1m2 0h1m2 0h1M6 9h1m2 0h1m2 0h1m2 0h1m2 0h1m2 0h1M8 10h1m2 0h1m2 0h1m2 0h1m2 0h1M7 11h1m2 0h1m2 0h1m2 0h1m2 0h1M9 12h1m2 0h1m2 0h1m2 0h1M8 13h1m2 0h1m2 0h1m2 0h1M7 14h1m2 0h1m2 0h1m2 0h1m-8 1h1m2 0h1m2 0h1m-8 1h1m2 0h1m2 0h1m-5 1h1m2 0h1m-5 1h1m2 0h1m-2 1h1m-2 1h1"
      />
      <path
        stroke="#124e89"
        d="M7 6h2m1 0h2m1 0h2m1 0h2m1 0h2m1 0h2m2 0h1M6 7h2m1 0h2m1 0h2m1 0h2m1 0h2m1 0h2m1 0h1m1 0h1M6 8h1m1 0h2m1 0h2m1 0h2m1 0h2m1 0h2m2 0h1m1 0h1M7 9h2m1 0h2m1 0h2m1 0h2m1 0h2m1 0h1m1 0h1m1 0h1M6 10h2m1 0h2m1 0h2m1 0h2m1 0h2m2 0h1m1 0h1m1 0h1M6 11h1m1 0h2m1 0h2m1 0h2m1 0h2m1 0h1m1 0h1m1 0h1m1 0h1M7 12h2m1 0h2m1 0h2m1 0h2m2 0h1m1 0h1m1 0h1M7 13h1m1 0h2m1 0h2m1 0h2m1 0h1m1 0h1m1 0h1m1 0h1M8 14h2m1 0h2m1 0h2m2 0h1m1 0h1m1 0h1m1 0h1M7 15h2m1 0h2m1 0h2m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1M9 16h2m1 0h2m2 0h1m1 0h1m1 0h1m1 0h1m1 0h1M8 17h2m1 0h2m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1M8 18h1m1 0h2m2 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1M9 19h2m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1M9 20h1m2 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1M9 21h2m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m-11 1h1m1 0h1m1 0h1m1 0h1m1 0h1m-9 1h1m1 0h1m1 0h1m1 0h1m1 0h1m-7 1h1m1 0h1m1 0h1m-5 1h1m1 0h1m1 0h1m-3 1h1"
      />
      <path
        stroke="#67c6ec"
        d="M25 6h1m-1 1h1m-3 1h1m1 0h1m-3 1h1m1 0h1m-5 1h1m1 0h1m1 0h1m-5 1h1m1 0h1m1 0h1m-7 1h1m1 0h1m1 0h1m1 0h1m-7 1h1m1 0h1m1 0h1m1 0h1m-9 1h1m1 0h1m1 0h1m1 0h1m1 0h1m-9 1h1m1 0h1m1 0h1m1 0h1m1 0h1m-11 1h1m1 0h1m1 0h1m1 0h1m1 0h1m-9 1h1m1 0h1m1 0h1m1 0h1m1 0h1m-11 1h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m-11 1h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m-13 1h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m-13 1h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m-11 1h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m-9 1h1m1 0h1m1 0h1m1 0h1m-7 1h1m1 0h1m1 0h1m1 0h1m-5 1h1m1 0h1m-3 1h1m1 0h1"
      />
      <path
        stroke="#686c6e"
        d="M23 21h1m-14 1h1m11 0h1m-12 1h1m9 0h1m-10 1h1m7 0h1m-8 1h1m5 0h1m-6 1h1m3 0h1"
      />
    </svg>
  );
}

export default SvgShield;
