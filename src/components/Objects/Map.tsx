import * as React from "react";

function SvgMap(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 -0.5 32 32"
      shapeRendering="crispEdges"
      {...props}
    >
      <path
        stroke="#733e39"
        d="M16 3h3m-4 1h1m3 0h1m-5 1h1m3 0h1m-7 1h2m5 0h1m-9 1h1m7 0h1M11 8h1m9 0h1M9 9h2m10 0h1M4 10h2m1 0h2m13 0h1M2 11h2m2 0h1m16 0h1M1 12h1m22 0h1M2 13h1m22 0h1M3 14h1m22 0h1M4 15h1m22 0h1M5 16h1m22 0h2M6 17h1m23 0h2M6 18h1m24 0h1M6 19h1m22 0h2M6 20h1m20 0h2M7 21h1m17 0h2M7 22h1m13 0h4M8 23h1m13 0h1M9 24h1m9 0h3m-12 1h2m5 0h2m-7 1h1m2 0h2m-5 1h3"
      />
      <path
        stroke="#ead4aa"
        d="M16 4h3m-3 1h3m-4 1h3m1 0h1m-7 1h2m1 0h1m1 0h2m-8 1h2m1 0h1m1 0h1m1 0h2M11 9h2m1 0h1m1 0h1m1 0h1m1 0h1M9 10h9m1 0h3M5 11h1m1 0h16M2 12h13m1 0h1m1 0h1m1 0h3M3 13h2m1 0h1m1 0h5m1 0h7m1 0h1M4 14h20M5 15h3m1 0h3m1 0h8m1 0h3M6 16h4m1 0h4m3 0h6M7 17h13m1 0h3M7 18h5m5 0h1m1 0h6M7 19h3m2 0h1m2 0h11M7 20h2m2 0h7m1 0h5M8 21h8m1 0h7M8 22h6m1 0h6M9 23h4m3 0h6m-12 1h4m1 0h4m-7 1h5m-4 1h2"
      />
      <path
        stroke="#3e8948"
        d="M18 6h1m-4 1h1m1 0h1m-4 1h1m1 0h1m1 0h1m-6 1h1m1 0h1m1 0h1m1 0h1m-2 1h1"
      />
      <path
        stroke="#a22633"
        d="M4 11h1m10 1h1m1 0h1m1 0h1M5 13h1m1 0h1m5 0h1m7 0h1M8 15h1m3 0h1m8 0h1m-12 1h1m9 1h1m-3 1h1m-1 2h1m-3 1h1m-3 1h1m-2 1h3m-2 1h1"
      />
      <path
        stroke="#124e89"
        d="M23 12h1m-1 1h2m-1 1h2m-1 1h2m-3 1h4m-4 1h6m-5 1h6m-5 1h3m-5 1h3m-3 1h1"
      />
      <path stroke="#3e2731" d="M15 16h3m-6 2h5m-7 1h2m1 0h2m-6 1h2" />
    </svg>
  );
}

export default SvgMap;
