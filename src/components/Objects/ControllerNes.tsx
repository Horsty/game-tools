import * as React from "react";

function SvgControllerNes(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 -0.5 32 32"
      shapeRendering="crispEdges"
      {...props}
    >
      <path
        stroke="#000"
        d="M7 6h5m7 0h5M6 7h1m5 0h7m5 0h1M5 8h1m19 0h1M4 9h1m3 0h2m16 0h1M4 10h1m3 0h2m16 0h1M4 11h1m1 0h6m14 0h1M4 12h1m1 0h6m2 0h1m2 0h1m8 0h1M4 13h1m3 0h2m3 0h1m2 0h1m9 0h1M4 14h1m3 0h2m16 0h1M4 15h1m21 0h1M4 16h1m6 0h9m6 0h1M5 17h1m4 0h1m9 0h1m4 0h1M6 18h4m11 0h4"
      />
      <path
        stroke="#cdcecb"
        d="M7 7h5m7 0h5M6 8h18M5 9h3m2 0h11m2 0h1M5 10h3m2 0h11m2 0h2M5 11h1m6 0h7m2 0h2M5 12h1m6 0h2m1 0h2m1 0h1m3 0h1M5 13h3m2 0h3m1 0h2m1 0h3m3 0h2M5 14h3m2 0h11m2 0h2M5 15h5m5 0h10M5 16h4m11 0h4M6 17h1m14 0h1"
      />
      <path
        stroke="#838483"
        d="M24 8h1m0 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-1 1h1m-16 1h5m10 0h1M9 16h2m13 0h2M7 17h3m12 0h3"
      />
      <path stroke="#0099db" d="M21 9h2m-2 1h2" />
      <path stroke="#3e8948" d="M19 11h2m-2 1h2" />
      <path stroke="#f04" d="M23 11h2m-2 1h2" />
      <path stroke="#fee761" d="M21 13h2m-2 1h2" />
    </svg>
  );
}

export default SvgControllerNes;
