import React from "react";
import SvgLaptop from "./Laptop";
import SvgBow from "./Bow";
import SvgSword from "./Sword";
import SvgControllerNes from "./ControllerNes";
import SvgGameboy from "./Gameboy";
import SvgMap from "./Map";
import SvgShield from "./Shield";
import SvgSwordLarge from "./SwordLarge";
import SvgSword2 from "./Sword2";

export default {
  title: "Items",
  component: SvgLaptop,
};

const defaultSize = 320;

export const Laptop = () => <SvgLaptop width={defaultSize} />;
export const Bow = () => <SvgBow width={defaultSize} />;
export const ControllerNes = () => <SvgControllerNes width={defaultSize} />;
export const GameBoy = () => <SvgGameboy width={defaultSize} />;
export const Map = () => <SvgMap width={defaultSize} />;
export const Shield = () => <SvgShield width={defaultSize} />;
export const Sword = () => <SvgSword width={defaultSize} />;
export const LargeSword = () => <SvgSwordLarge width={defaultSize} />;
export const Sword2 = () => <SvgSword2 width={defaultSize} />;
