import * as React from "react";

function SvgLaptop(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 -0.5 32 32"
      shapeRendering="crispEdges"
      {...props}
    >
      <path
        stroke="#000"
        d="M5 4h10m1 0h10M5 5h1m19 0h1M5 6h1m1 0h17m1 0h1M5 7h1m1 0h1m15 0h1m1 0h1M5 8h1m1 0h1m1 0h1m1 0h1m1 0h1m9 0h1m1 0h1M5 9h1m1 0h1m15 0h1m1 0h1M5 10h1m1 0h1m1 0h1m1 0h1m11 0h1m1 0h1M5 11h1m1 0h1m15 0h1m1 0h1M5 12h1m1 0h1m15 0h1m1 0h1M5 13h1m1 0h1m15 0h1m1 0h1M5 14h1m1 0h1m15 0h1m1 0h1M5 15h1m1 0h1m15 0h1m1 0h1M5 16h1m1 0h17m1 0h1M5 17h1m19 0h1M6 18h19M5 19h1m19 0h1M4 20h1m21 0h1M3 21h1m23 0h1M2 22h1m25 0h1M1 23h1m27 0h1M1 24h29M1 25h2m1 0h1m1 0h24M2 27h27"
      />
      <path stroke="#2a2a29" d="M15 4h1" />
      <path
        stroke="#a9aaa8"
        d="M6 5h19M6 6h1m17 0h1M6 7h1m17 0h1M6 8h1m17 0h1M6 9h1m17 0h1M6 10h1m17 0h1M6 11h1m17 0h1M6 12h1m17 0h1M6 13h1m17 0h1M6 14h1m17 0h1M6 15h1m17 0h1M6 16h1m17 0h1M6 17h19M8 20h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1M7 21h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1M6 22h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1"
      />
      <path
        stroke="#67c6ec"
        d="M8 7h15M8 8h1m1 0h1m1 0h1m1 0h3m1 0h1m3 0h1M8 9h13m1 0h1M8 10h1m1 0h1m1 0h11M8 11h15M8 12h15M9 13h14m-12 1h11"
      />
      <path stroke="#fff" d="M17 8h1m1 0h3m-1 1h1" />
      <path stroke="#3e8948" d="M8 13h1m-1 1h3m11 0h1M8 15h15" />
      <path
        stroke="#cdcecb"
        d="M6 19h19M5 20h2m17 0h2M4 21h2m19 0h2M3 22h2m21 0h2M2 23h27"
      />
      <path
        stroke="#838483"
        d="M7 20h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1M6 21h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1M5 22h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1m1 0h1"
      />
      <path stroke="#0099db" d="M3 25h1m1 0h1" />
      <path stroke="#686c6e" d="M2 26h27" />
    </svg>
  );
}

export default SvgLaptop;
