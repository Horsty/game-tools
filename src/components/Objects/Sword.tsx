import * as React from "react";

function SvgSword(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 -0.5 32 32"
      shapeRendering="crispEdges"
      {...props}
    >
      <path
        stroke="#000"
        d="M22 6h3m-4 1h1m2 0h1m-5 1h1m3 0h1m-6 1h1m3 0h1m-6 1h1m3 0h1m-6 1h1m3 0h1m-6 1h1m3 0h1m-6 1h1m3 0h1m-6 1h1m3 0h1m-9 1h4m3 0h1m-8 1h1m2 0h1m2 0h1m-6 1h1m1 0h3m-5 1h2m2 0h1m-6 1h1m1 0h2m1 0h1m-7 1h1m1 0h1m2 0h2m-9 1h2m1 0h1m-4 1h1m1 0h1m-3 1h3"
      />
      <path
        stroke="#fff"
        d="M22 7h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1"
      />
      <path
        stroke="#89bdd9"
        d="M23 7h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1"
      />
      <path
        stroke="#c0cbdc"
        d="M23 8h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1m-2 1h1"
      />
      <path stroke="#feae34" d="M11 16h2m-1 1h1m0 1h2m-1 1h1" />
      <path stroke="#733e39" d="M11 19h1m-2 1h1m-2 1h1" />
      <path stroke="#a22633" d="M8 22h1" />
    </svg>
  );
}

export default SvgSword;
