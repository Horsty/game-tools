import * as React from "react";

function SvgGameboy(props: React.SVGProps<SVGSVGElement>) {
  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      viewBox="0 -0.5 32 32"
      shapeRendering="crispEdges"
      {...props}
    >
      <path
        stroke="#000"
        d="M7 3h18M6 4h1m18 0h1M6 5h1m18 0h1M6 6h1m18 0h1M6 7h1m18 0h1M6 8h1m18 0h1M6 9h1m18 0h1M6 10h1m18 0h1M6 11h1m18 0h1M6 12h1m18 0h1M6 13h1m18 0h1M6 14h1m18 0h1M6 15h1m18 0h1M6 16h1m18 0h1M6 17h1m18 0h1M6 18h1m18 0h1M6 19h1m3 0h1m14 0h1M6 20h1m2 0h1m1 0h1m13 0h1M6 21h1m3 0h1m14 0h1M6 22h1m18 0h1M6 23h1m18 0h1M6 24h1m18 0h1M6 25h1m18 0h1M6 26h1m17 0h1M6 27h1m16 0h1M6 28h1m15 0h1M7 29h15"
      />
      <path
        stroke="#cdcecb"
        d="M7 4h18M7 5h1m16 0h1M7 6h1m16 0h1M7 7h1m16 0h1M7 8h1m16 0h1M7 9h1m16 0h1M7 10h1m16 0h1M7 11h1m16 0h1M7 12h1m16 0h1M7 13h1m16 0h1M7 14h1m16 0h1M7 15h1m16 0h1M7 16h1m16 0h1M7 17h18M7 18h18M7 19h3m1 0h10m2 0h2M7 20h2m3 0h9m2 0h2M7 21h3m1 0h7m2 0h5M7 22h11m2 0h5M7 23h15m1 0h2M7 24h5m1 0h1m1 0h6m1 0h1m1 0h1M7 25h4m1 0h1m1 0h6m1 0h1m1 0h1M7 26h12m1 0h1m1 0h1M7 27h13m1 0h1M7 28h14"
      />
      <path
        stroke="#686c6e"
        d="M8 5h16M8 6h3m10 0h3M8 7h3m10 0h3M8 8h3m10 0h3M8 9h1m1 0h1m10 0h3M8 10h3m10 0h3M8 11h3m10 0h3M8 12h3m10 0h3M8 13h3m10 0h3M8 14h3m10 0h3M8 15h3m10 0h3M8 16h16"
      />
      <path
        stroke="#88b852"
        d="M11 6h10M11 7h10M11 8h10M11 9h10m-10 1h10m-10 1h10m-10 1h10m-10 1h10m-10 1h10m-10 1h10"
      />
      <path stroke="#f04" d="M9 9h1" />
      <path stroke="#b55088" d="M21 19h2m-2 1h2m-5 1h2m-2 1h2" />
      <path stroke="#2a2a29" d="M10 20h1" />
      <path
        stroke="#a9aaa8"
        d="M22 23h1m-11 1h1m1 0h1m6 0h1m1 0h1m-13 1h1m1 0h1m6 0h1m1 0h1m1 0h1m-6 1h1m1 0h1m1 0h1m-4 1h1m1 0h1m-2 1h1"
      />
    </svg>
  );
}

export default SvgGameboy;
