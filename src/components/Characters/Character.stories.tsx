import React from "react";
import Character from "./Character";

// import spritesheet from "../../assets/spritesheet/spritesheet_centre_32.png";
import catSheet from "../../assets/spritesheet/cat_sprite_256b.png";
import testTete from "../../assets/spritesheet/test.png";
import testW from "../../assets/spritesheet/test_w.png";

// customization
import top from "../../assets/customization/top_m_shirt_batman.png";
import glasses from "../../assets/customization/sunglasses.png";
import face from "../../assets/customization/face_laughing.png";
import pant from "../../assets/customization/pants_basic.png";
import hair from "../../assets/customization/hair_punk.png";
import accessory from "../../assets/customization/backpack.png";
import basicCharacter from "../../assets/customization/basic_character.png";

import basicHaircurt from "../../assets/customization/hair_m_short.png";

export default {
  title: "Character",
  component: Character,
};

const fullCustomCharacter = {
  haircut: hair,
  face: face,
  clothes: top,
  pants: pant,
  styleItem: glasses,
  accessory: accessory,
  spritesheet: basicCharacter,
};

const batman = {
  haircut: basicHaircurt,
  spritesheet: basicCharacter,
  accessory: accessory,
  clothes: top,
  pants: pant,
  styleItem: glasses,
};

export const Batman = () => (
  <Character {...batman} direction="down" />
);

/** Full custom */
export const fullCustom = () => (
  <Character {...fullCustomCharacter} direction="down" />
);
export const fullCustomLeft = () => (
  <Character {...fullCustomCharacter} direction="left" />
);
export const fullCustomRight = () => (
  <Character {...fullCustomCharacter} direction="right" />
);
export const fullCustomUp = () => (
  <Character {...fullCustomCharacter} direction="up" />
);

export const fullCustomMoving = () => (
  <Character {...fullCustomCharacter} direction="down" move={true} />
);
export const fullCustomLeftMoving = () => (
  <Character {...fullCustomCharacter} direction="left" move={true} />
);
export const fullCustomRightMoving = () => (
  <Character {...fullCustomCharacter} direction="right" move={true} />
);
export const fullCustomUpMoving = () => (
  <Character {...fullCustomCharacter} direction="up" move={true} />
);

export const StaticDown = () => (
  <Character spritesheet={testTete} pet={catSheet} direction="down" />
);
export const StaticUp = () => (
  <Character spritesheet={testTete} pet={catSheet} direction="up" />
);
export const StaticLeft = () => (
  <Character spritesheet={testTete} pet={catSheet} direction="left" />
);
export const StaticRight = () => (
  <Character spritesheet={testTete} pet={catSheet} direction="right" />
);
export const MovingDownWithPet = () => (
  <Character
    spritesheet={testTete}
    pet={catSheet}
    direction="down"
    move={true}
  />
);
export const MovingUpWithPet = () => (
  <Character spritesheet={testTete} pet={catSheet} direction="up" move={true} />
);
export const MovingLeftWithPet = () => (
  <Character
    spritesheet={testTete}
    pet={catSheet}
    direction="left"
    move={true}
  />
);
export const MovingRightWithPet = () => (
  <Character
    spritesheet={testTete}
    pet={catSheet}
    direction="right"
    move={true}
  />
);
export const MovingDown = () => (
  <Character spritesheet={testTete} move={true} />
);
export const MovingRight = () => (
  <Character spritesheet={testTete} direction="right" move={true} />
);
export const MovingUp = () => (
  <Character spritesheet={testTete} direction="up" move={true} />
);
export const MovingLeft = () => (
  <Character spritesheet={testTete} direction="left" move={true} />
);

export const StaticPet = () => <Character spritesheet={catSheet} />;
export const MovingUpPet = () => (
  <Character direction="up" move={true} spritesheet={catSheet} />
);
export const MovingDownPet = () => (
  <Character direction="down" move={true} spritesheet={catSheet} />
);
export const MovingRightPet = () => (
  <Character direction="right" move={true} spritesheet={catSheet} />
);
export const MovingLeftPet = () => (
  <Character direction="left" move={true} spritesheet={catSheet} />
);

export const StaticWoman = () => <Character spritesheet={testW} />;
export const MovingDownW = () => <Character spritesheet={testW} move={true} />;
export const MovingRightW = () => (
  <Character spritesheet={testW} direction="right" move={true} />
);
export const MovingUpW = () => (
  <Character spritesheet={testW} direction="up" move={true} />
);
export const MovingLeftW = () => (
  <Character spritesheet={testW} direction="left" move={true} />
);
