import React from "react";
import PropTypes from "prop-types";
import shadow from "../../assets/images/CharacterShadow.png";
import styles from "./Character.module.scss";

const Character = ({
  direction,
  move,
  spritesheet,
  haircut,
  face,
  clothes,
  pants,
  accessory,
  styleItem,
  pet,
  withShadow = true,
}) => {
  const getImg = (
    spriteSheetName: string,
    spriteSheet: string,
    additionalClass = ""
  ) => {
    return (
      <img
        className={`${styles[`${spriteSheetName}_spritesheet`]} ${
          move ? styles.move : ""
        } ${additionalClass ? styles[additionalClass] : ""} ${
          styles.pixelart
        } ${styles[`face-${direction}`]}`}
        src={spriteSheet}
        alt={spriteSheetName.toUpperCase()}
      />
    );
  };
  return (
    <div className={styles.character}>
      {withShadow ? (
        <img
          className={`${styles.character_shadow} ${styles.pixelart}`}
          src={shadow}
          alt="Shadow"
        />
      ) : null}
      {spritesheet ? getImg("character", spritesheet) : null}
      {haircut ? getImg("haircut", haircut, "top-level") : null}
      {face ? getImg("face", face) : null}
      {clothes ? getImg("clothes", clothes) : null}
      {pants ? getImg("pants", pants) : null}
      {accessory ? getImg("accessory", accessory) : null}
      {styleItem ? getImg("styleItem", styleItem) : null}
      {pet ? getImg("pet", pet, "customFront") : null}
    </div>
  );
};

Character.defaultProps = {
  direction: "down",
  move: false,
};
Character.propTypes = {
  direction: PropTypes.string,
  move: PropTypes.bool,
  spritesheet: PropTypes.string,
  haircut: PropTypes.string,
  face: PropTypes.string,
  clothes: PropTypes.string,
  pants: PropTypes.string,
  accessory: PropTypes.string,
  styleItem: PropTypes.string,
  pet: PropTypes.string,
  withShadow: PropTypes.bool,
};
export default Character;
