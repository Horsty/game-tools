import React from "react";
import PropTypes from "prop-types";
import styles from "./Color.module.scss";

const Color = ({ type }) => {
  const hexaColor = getComputedStyle(document.documentElement).getPropertyValue(
    `--${type}`
  );
  return (
    <div className={styles.colorBox}>
      <div className={`${styles.color} ${styles[`color--${type}`]}`}>
        {hexaColor}
      </div>
      <p className={styles.text}>
        {type} : {hexaColor}
      </p>
    </div>
  );
};
Color.defaultProps = {
  type: "blue",
};
Color.propTypes = {
  type: PropTypes.string,
};

export default Color;
