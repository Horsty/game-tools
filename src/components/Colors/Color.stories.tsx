import React from "react";
import Color from "./Color";

export default {
  title: "Available Colors",
  component: Color,
};

export const exemple = () => <Color type="blue" />;
export const info = () => <Color type="blue" />;
export const alert = () => <Color type="orange" />;
export const black = () => <Color type="black" />;
export const disabled = () => <Color type="gray" />;
export const placeholder = () => <Color type="gray-light" />;
export const success = () => <Color type="green" />;
export const white = () => <Color type="white" />;
