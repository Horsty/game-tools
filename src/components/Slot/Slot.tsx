import React from "react";
import styles from "./Slot.module.scss";
import PropTypes from "prop-types";
import SvgLaptop from "../Objects/Laptop";

const InventorySlot = ({ item, name }) => {
  return (
    <div className={styles.card}>
      <div className={styles["image-container"]}>
        <svg
          className={styles.shadow}
          viewBox="0 0 156 73"
          fill="none"
          xmlns="http://www.w3.org/2000/svg"
        >
          <ellipse
            id="shadow"
            cx="78"
            cy="36.5"
            rx="78"
            ry="36.5"
            fill="#07123C"
            fillOpacity="0.19"
          />
        </svg>
        <div className={styles.item}>{item()}</div>
      </div>
      <span className={styles["item-title"]}>{name}</span>
    </div>
  );
};

InventorySlot.defaultProps = {
  item: SvgLaptop({ width: 320 }),
  name: "Laptop",
};
InventorySlot.propTypes = {
  item: PropTypes.func,
  name: PropTypes.string,
};
export default InventorySlot;
