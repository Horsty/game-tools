import React from "react";
import SvgBow from "../Objects/Bow";
import SvgControllerNes from "../Objects/ControllerNes";
import SvgGameboy from "../Objects/Gameboy";
import SvgLaptop from "../Objects/Laptop";
import SvgMap from "../Objects/Map";
import SvgShield from "../Objects/Shield";
import SvgSword from "../Objects/Sword";
import InventorySlot from "./Slot";

export default {
  title: "Inventory Slot",
  component: InventorySlot,
};

export const Laptop = () => (
  <InventorySlot item={() => SvgLaptop({ width: 32 })} name={"Laptop"} />
);

export const Bow = () => (
  <InventorySlot item={() => SvgBow({ width: 32 })} name={"Bow"} />
);

export const ControllerNes = () => (
  <InventorySlot
    item={() => SvgControllerNes({ width: 32 })}
    name={"ControllerNes"}
  />
);

export const GameBoy = () => (
  <InventorySlot item={() => SvgGameboy({ width: 32 })} name={"GameBoy"} />
);

export const Map = () => (
  <InventorySlot item={() => SvgMap({ width: 32 })} name={"Map"} />
);

export const Shield = () => (
  <InventorySlot item={() => SvgShield({ width: 32 })} name={"Shield"} />
);

export const Sword = () => (
  <InventorySlot item={() => SvgSword({ width: 32 })} name={"Sword"} />
);
