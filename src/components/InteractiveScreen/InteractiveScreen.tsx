import React, { useEffect, useRef } from "react";
import styles from "./InteractiveScreen.module.scss";
import PropTypes from "prop-types";
import SvgLaptop from "../Objects/Laptop";

const InteractiveScreen = ({ player, background }) => {
  let root = document.documentElement;
  const walkingAnimation = "walkAnimation 0.6s steps(4) infinite";

  //start in the middle of the map
  var x = 90;
  var y = 34;
  var held_directions = []; //State of which arrow keys we are holding down
  var speed = 1; //How fast the character moves in pixels per frame
  let direction;

  var pixelSize = parseInt(
    getComputedStyle(document.documentElement).getPropertyValue("--pixel-size")
  );

  /* Direction key state */
  const directions = {
    up: "up",
    down: "down",
    left: "left",
    right: "right",
  };

  const keys = {
    ArrowUp: directions.up,
    ArrowLeft: directions.left,
    ArrowRight: directions.right,
    ArrowDown: directions.down,
  };

  const faceShow = {
    [directions.right]: "calc(var(--pixel-size) * -32)",
    [directions.up]: "calc(var(--pixel-size) * -64)",
    [directions.left]: "calc(var(--pixel-size) * -96)",
  };

  document.addEventListener("keydown", (e) => {
    var dir = keys[e.code];
    if (dir && held_directions.indexOf(dir) === -1) {
      held_directions.unshift(dir);
    }
  });

  document.addEventListener("keyup", (e) => {
    var dir = keys[e.code];
    var index = held_directions.indexOf(dir);
    if (index > -1) {
      held_directions.splice(index, 1);
    }
  });
  const laptop = {
    x: 64,
    y: 64,
    width: 64,
    height: 64,
  };
  const myObstacle = useRef(null);
  useEffect(() => {
    const obstacle = myObstacle.current;
    if (obstacle) {
      laptop.width = obstacle.clientWidth / pixelSize;
      laptop.height = obstacle.clientHeight / pixelSize;
      setObstacle(laptop.x, laptop.y, obstacle);
    }
  }, []);

  const placeCharacter = () => {
    const previousPositionX = x;
    const previousPositionY = y;
    const held_direction = held_directions[0];
    if (held_direction) {
      if (held_direction === directions.right) {
        x += speed;
      }
      if (held_direction === directions.left) {
        x -= speed;
      }
      if (held_direction === directions.down) {
        y += speed;
      }
      if (held_direction === directions.up) {
        y -= speed;
      }
      direction = held_direction;
      root.style.setProperty("--position-y", faceShow[direction]);
    }
    root.style.setProperty(
      "--character-animation",
      held_direction ? walkingAnimation : ""
    );
    root.style.setProperty("--background", `url(${background})`);

    //Limits (gives the illusion of walls)
    var leftLimit = -8;
    var rightLimit = 16 * 11 + 8;
    var topLimit = -8 + 32;
    var bottomLimit = 16 * 7;

    var camera_left = pixelSize * 66;
    var camera_top = pixelSize * 42;
    if (
      x > laptop.x + laptop.width / pixelSize || // position x is to the right of obstacle right
      x + 64 / pixelSize < laptop.x || // character right is to the left of obstacle left
      y > laptop.y + laptop.height / pixelSize || // position y is below obstacle bottom
      y + 64 / pixelSize < laptop.y // character bottom is above obstacle top
    ) {
      // no collision
      if (x < leftLimit) {
        x = leftLimit;
      }

      if (x > rightLimit) {
        x = rightLimit;
      }

      if (y < topLimit) {
        y = topLimit;
      }

      if (y > bottomLimit) {
        y = bottomLimit;
      }
    } else {
      // colission
      x = previousPositionX;
      y = previousPositionY;
    }
    root.style.setProperty(
      "--transform-map",
      `translate3d( ${-x * pixelSize + camera_left}px, ${
        -y * pixelSize + camera_top
      }px, 0 )`
    );

    // set character to the middle of the map
    root.style.setProperty(
      "--transform-character",
      `translate3d( ${x * pixelSize}px, ${y * pixelSize}px, 0 )`
    );
  };

  //https://stackoverflow.com/questions/54633690/how-can-i-use-multiple-refs-for-an-array-of-elements-with-hooks
  const setObstacle = (x: number, y: number, obstacle) => {
    obstacle.style.transform = `translate3d( ${x * pixelSize}px, ${
      y * pixelSize
    }px, 0 )`;
  };

  //Set up the game loop
  const step = () => {
    placeCharacter();
    window.requestAnimationFrame(() => {
      step();
    });
  };
  step(); //kick off the first step!

  return (
    <div className={styles.frame}>
      <br />
      <div className={styles.camera}>
        <div className={`${styles.map} ${styles["pixel-art"]}`}>
          <div className={`${styles.obstacles}`} ref={myObstacle}>
            <SvgLaptop width={64} />
          </div>
          <div className={styles.character}>{player ? player() : null}</div>
        </div>
      </div>
    </div>
  );
};

InteractiveScreen.defaultProps = {
  player: null,
  // obstacles: [],
};

InteractiveScreen.prototype = {
  player: PropTypes.func,
  // obstacles: PropTypes.array,
};

export default InteractiveScreen;

// export interface IObstacle {
//   x: number;
//   y: number;
//   sprite: string;
// }
