import React from "react";
import InteractiveScreen from "./InteractiveScreen";
import backGround from "./../../assets/images/CameraDemoMap.png";
import sprite from "../../assets/spritesheet/test.png";
import shadow from "../../assets/images/CharacterShadow.png";
import Character from "../Characters/Character";

export default {
  title: "Interactive Screen",
  component: InteractiveScreen,
};
const player = (setting) =>
  Character({ ...setting, spritesheet: sprite, shadow });
export const example = () => (
  <InteractiveScreen player={player} background={backGround} />
);
