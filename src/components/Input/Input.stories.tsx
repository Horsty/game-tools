import * as React from "react";
import TextInput from "./Input";

export default {
  title: "Input",
  component: TextInput,
};
/**
 * TextInput
 */
export const example = (state, setState) => (
  <TextInput
    label="Nom de la carte"
    name="name"
    placeholder="Entrez ici le nom de la carte"
    value={state.value}
    onChange={(e) => setState({ value: e.target.value })}
  />
);
export const onError = (state, setState) => (
  <TextInput
    label="Nom de la carte"
    name="name"
    placeholder="Entrez ici le nom de la carte"
    value={state.value}
    error="Il y a une erreur"
    onChange={(e) => setState({ value: e.target.value })}
  />
);
