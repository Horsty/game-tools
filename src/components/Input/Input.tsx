import React from "react";
import PropTypes from "prop-types";
import "./Input.scss";

import SvgSearch from "../Icons/Search";
import { InputProps } from './Input.types';

const TextInput: React.FC<InputProps> = ({
  name,
  label,
  onChange,
  placeholder,
  value,
  error,
  color,
  onKeyDown,
}) => {
  let wrapperClass = "form-group";
  if (error && error.length > 0) {
    wrapperClass += " has-error";
  }
  const hexaColor = getComputedStyle(document.documentElement).getPropertyValue(
    `--${color}`
  );
  return (
    <div className={`textInput ${wrapperClass}`}>
      {label ? <label htmlFor={name}>{label}</label> : null}
      <div className="field">
        <input
          autoComplete="off"
          type="text"
          name={name}
          className="form-control"
          placeholder={placeholder}
          value={value}
          onChange={onChange}
          onKeyDown={onKeyDown}
        />
        <SvgSearch className="placeholderIcon" fill={hexaColor} />
        {error && <div className="alert alert-danger">{error}</div>}
      </div>
    </div>
  );
};
TextInput.defaultProps = {
  color: "gray-light",
};
TextInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  placeholder: PropTypes.string,
  value: PropTypes.string,
  error: PropTypes.string,
  color: PropTypes.string,
};

export default TextInput;
