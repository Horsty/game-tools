export interface InputProps {
  name: string,
  label: string,
  onChange: any,
  onKeyDown?: any,
  placeholder: string,
  value: string,
  error?: string,
  color?: string,
}
