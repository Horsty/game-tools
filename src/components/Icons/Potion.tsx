import React from "react";
export const MagicJar = ({width}) => (
    <svg width={width} xmlns="http://www.w3.org/2000/svg" viewBox="0 -0.5 7 9" shapeRendering="crispEdges">
      <metadata>Made with Pixels to Svg https://codepen.io/shshaw/pen/XbxvNj</metadata>
      <path stroke="#213d05" d="M2 0h3M1 1h1M5 1h1M2 2h1M4 2h1M2 3h1M4 3h1M2 4h1M4 4h1M1 5h1M5 5h1M0 6h1M6 6h1M1 7h1M5 7h1M2 8h3" />
      <path stroke="#c9ff94" d="M2 1h1M2 5h1M1 6h1" />
      <path stroke="#76e111" d="M3 1h2M3 2h1M4 5h1M3 6h2" />
      <path stroke="rgba(64,64,0,0.01568627450980392)" d="M1 2h1M5 2h1M5 3h1M5 4h1M0 5h1M6 5h1" />
      <path stroke="#c0be20" d="M3 3h1" />
      <path stroke="#fffc4d" d="M3 4h1" />
      <path stroke="#91f530" d="M3 5h1M2 6h1M2 7h1" />
      <path stroke="#5dc202" d="M5 6h1M3 7h2" />
    </svg>
  );