import React from "react";
import SvgCross from "./Cross";
import SvgSearch from "./Search";
import SvgHeart from "./Heart";

export default {
  title: "Available Icons",
  component: SvgCross,
};

export const Cross = () => <SvgCross width="19" height="19" />;
export const Search = () => <SvgSearch />;
export const Heart = () => <SvgHeart fill={1} />;
export const HalfHeart = () => <SvgHeart fill={0.5} />;
